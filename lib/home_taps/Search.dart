import 'package:flutter/material.dart';
import 'package:maravel/screens/ServiceProviderScreen.dart';
import 'package:maravel/widget/CustomButton.dart';
import 'package:maravel/widget/CustomLabel.dart';
import 'package:maravel/widget/CustomTextField.dart';
import 'package:maravel/widget/CustomTitle.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {

  void onButtonPressed(){
    showMaterialModalBottomSheet(
      context: context,
      builder: (context) => SingleChildScrollView(
        controller: ModalScrollController.of(context),
        child: Column(
          children: [
            Text('Choose Number Of Person', style: TextStyle(color: Colors.red, fontSize: 20,),),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Member',style: TextStyle(color: Colors.amberAccent, fontSize: 20),),
                  SizedBox(width: 40,),
                  CircleAvatar(child: Text('+'),backgroundColor: Colors.amberAccent,),
                  SizedBox(width: 70,),
                  Flexible(
                    child: new TextField(
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(
                                color: Colors.brown.withOpacity(0.5)),
                          ),
                          hintText: '1',
                          hintStyle: TextStyle(fontSize: 14),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 5),
                        )),
                  ),
                  SizedBox(width: 70,),
                  CircleAvatar(child: Text('-'),backgroundColor: Colors.grey,),
                ],
              ),
            ),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Kids',style: TextStyle(color: Colors.amberAccent, fontSize: 20),),
                  SizedBox(width: 75,),
                  CircleAvatar(child: Text('+'),backgroundColor: Colors.amberAccent,),
                  SizedBox(width: 70,),
                  Flexible(
                    child: new TextField(
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(
                                color: Colors.brown.withOpacity(0.5)),
                          ),
                          hintText: '1',
                          hintStyle: TextStyle(fontSize: 14),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 5),
                        )),
                  ),
                  SizedBox(width: 70,),
                  CircleAvatar(child: Text('-'),backgroundColor: Colors.grey,),
                ],
              ),
            ),
            SizedBox(height: 20,),
            CustomButton(text: 'Confirm', color: Colors.amberAccent, function: (){
              Navigator.pop(context);
            },),

          ],
        ),
      ),
    );
  }
  int _currentIndex = 0;
  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  void datePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime(2021),
      firstDate: DateTime(1800),
      lastDate: DateTime(2050),
    );
  }
  bool firstValue = false;

  bool secondValue = false;

  bool thirdValue = false;

  bool fourthValue = false;

  bool fifthValue = false;

  bool sexValue = false;

  final imageList = [
    'https://cdn.pixabay.com/photo/2016/03/05/19/02/hamburger-1238246__480.jpg',
    'https://cdn.pixabay.com/photo/2016/11/20/09/06/bowl-1842294__480.jpg',
    'https://cdn.pixabay.com/photo/2017/01/03/11/33/pizza-1949183__480.jpg',
    'https://cdn.pixabay.com/photo/2017/02/03/03/54/burger-2034433__480.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        CustomTitle(text: 'بحث',),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.all(17),
          child: Container(
            height: 150,
            child: Swiper(
              pagination: new SwiperPagination(),
              autoplay: true,
              itemWidth: 100,
              itemCount: imageList.length,
              itemBuilder: (context, index) {
                return Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: NetworkImage(imageList[index]),
                        fit: BoxFit.fill,
                      )),
                );
              },
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: EdgeInsets.only(
            right: 20,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                'اختر القسم',
                style: TextStyle(fontSize: 20),
              ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 60),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'عمره',
                    style: TextStyle(fontSize: 20),
                  ),
                  Checkbox(
                    checkColor: Colors.white,
                    activeColor: Colors.amberAccent,
                    value: firstValue,
                    onChanged: (bool value) {
                      setState(() {
                        firstValue = value;
                      });
                    },
                  ),
                ],
              ),
            ),
            Row(
              // mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 210),
                  child: Text(
                    'حج',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Checkbox(
                  checkColor: Colors.white,
                  activeColor: Colors.amberAccent,
                  value: secondValue,
                  onChanged: (bool value) {
                    setState(() {
                      secondValue = value;
                    });
                  },
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        CustomLabel(
          text: 'الدوله',
        ),
        SizedBox(
          height: 10,
        ),
        CustomTextField(
          text: 'الرجاء ادخال الدوله',
          prefixIcon: Icon(Icons.arrow_drop_down),
        ),
        SizedBox(
          height: 10,
        ),
        CustomLabel(
          text: 'المدينه',
        ),
        SizedBox(
          height: 10,
        ),
        CustomTextField(
          text: 'الرجاء ادخال المدينه',
          prefixIcon: Icon(Icons.arrow_drop_down),
        ),
        SizedBox(
          height: 10,
        ),
        CustomLabel(
          text: 'نوع الرحله',
        ),
        SizedBox(
          height: 10,
        ),
        CustomTextField(
          text: 'الرجاء ادخال نوع الرحله',
          prefixIcon: Icon(Icons.arrow_drop_down),
        ),
        SizedBox(
          height: 10,
        ),
        CustomLabel(
          text: 'وسيله النقل',
        ),
        SizedBox(
          height: 10,
        ),
        CustomTextField(
          text: 'الرجاء ادخال نوع السياره',
          prefixIcon: Icon(Icons.arrow_drop_down),
        ),
        SizedBox(
          height: 10,
        ),
        CustomLabel(
          text: 'تاريخ الوصول',
        ),
        SizedBox(
          height: 10,
        ),
        InkWell(
          onTap: () => datePicker(),
          child: AbsorbPointer(
            child: CustomTextField(
              text: 'الرجاء ادخال التاريخ',
              prefixIcon: Icon(Icons.calendar_today_outlined),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        CustomLabel(
          text: 'تاريخ المغادره',
        ),
        SizedBox(
          height: 10,
        ),
        InkWell(
          onTap: () => datePicker(),
          child: AbsorbPointer(
            child: CustomTextField(
              text: 'الرجاء ادخال التاريخ',
              prefixIcon: Icon(Icons.calendar_today_outlined),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        CustomLabel(
          text: 'عدد النزلاء',
        ),
        SizedBox(
          height: 10,
        ),
        InkWell(
          onTap: (){onButtonPressed();},
          child: AbsorbPointer(
            child: CustomTextField(
              text: 'الرجاء ادخال العدد',
            ),
          ),
          // onTap: createAlertDialog(context),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: EdgeInsets.only(
            right: 20,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                'استخدام قطار الشاعر',
                style: TextStyle(fontSize: 20),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 60),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'لا',
                    style: TextStyle(fontSize: 20),
                  ),
                  Checkbox(
                    checkColor: Colors.white,
                    activeColor: Colors.amberAccent,
                    value: thirdValue,
                    onChanged: (bool value) {
                      setState(() {
                        thirdValue = value;
                      });
                    },
                  ),
                ],
              ),
            ),
            Row(
              // mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 210),
                  child: Text(
                    'نعم',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Checkbox(
                  checkColor: Colors.white,
                  activeColor: Colors.amberAccent,
                  value: fourthValue,
                  onChanged: (bool value) {
                    setState(() {
                      fourthValue = value;
                    });
                  },
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: EdgeInsets.only(
            right: 20,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                'هل ترغب في الهدي (الأضحيه)',
                style: TextStyle(fontSize: 20),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 60),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'لا',
                    style: TextStyle(fontSize: 20),
                  ),
                  Checkbox(
                    checkColor: Colors.white,
                    activeColor: Colors.amberAccent,
                    value: fifthValue,
                    onChanged: (bool value) {
                      setState(() {
                        fifthValue = value;
                      });
                    },
                  ),
                ],
              ),
            ),
            Row(
              // mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 210),
                  child: Text(
                    'نعم',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Checkbox(
                  checkColor: Colors.white,
                  activeColor: Colors.amberAccent,
                  value: sexValue,
                  onChanged: (bool value) {
                    setState(() {
                      sexValue = value;
                    });
                  },
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        CustomButton(
          text: 'بحث',
          function: () {
            Navigator.push(context, MaterialPageRoute(builder: (context){
              return ServiceProviderScreen();
            }));
          },
          color: Colors.amberAccent,
        ),
        SizedBox(
          height: 20,
        ),

        //DefaultTabController(length: myTabs.length, child: Scaffold(appBar: AppBar(bottom: TabBar(tabs: myTabs,),),))
      ],
    );
  }
}
