import 'package:flutter/material.dart';
import 'package:maravel/screens/AboutUsScreeen.dart';
import 'package:maravel/screens/ChangeLanguage.dart';
import 'package:maravel/screens/ConditionsScreen.dart';
import 'package:maravel/screens/ContactUsScreen.dart';
import 'package:maravel/screens/ProfilePersonlyScreen.dart';
import 'package:maravel/widget/CustomTitle.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bground.png'), fit: BoxFit.fill),
        ),
        // margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 70),
              child: ListTile(
                // contentPadding: EdgeInsets.fromLTRB(100.0, 0.0, 10.0, 0.0),
                leading: CircleAvatar(
                  radius: 40,
                  backgroundImage: AssetImage('images/ahmed.jpg'),
                ),
                title: Text(
                  'Welcome Ahmed',
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 10,
                  ),
                ),
                subtitle: Text(
                  'ahmed.mail.com',
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 10.0,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'My Account',
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white54,
                borderRadius: BorderRadius.circular(20),
              ),
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ListTile(
                leading: Icon(Icons.person),
                trailing: IconButton(icon: Icon(Icons.arrow_forward), onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return ProfilePersonlyScreen();
                  }));
                },),
                title: Text(
                  'Profile personly',
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white54,
                borderRadius: BorderRadius.circular(20),
              ),
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ListTile(
                leading: Icon(Icons.account_balance_rounded),
                trailing: IconButton(icon: Icon(Icons.arrow_forward), onPressed: (){},),
                title: Text(
                  'Personal accounts',
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Settings',
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white54,
                borderRadius: BorderRadius.circular(20),
              ),
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ListTile(
                leading: Icon(Icons.notification_important),
                trailing: Switch(
                  value: isSwitched,
                  onChanged: (value) {
                    setState(() {
                      isSwitched = value;
                      print(isSwitched);
                    });
                  },
                  activeTrackColor: Colors.lightGreenAccent,
                  activeColor: Colors.green,
                  inactiveTrackColor: Colors.grey,
                ),
                title: Text(
                  'Notification',
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white54,
                borderRadius: BorderRadius.circular(20),
              ),
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ListTile(
                leading: Icon(Icons.language),
                trailing: InkWell(
                  onTap: () {
                    // Navigator.of(context).push('/ChangeLanguage');
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ChangeLanguage()));
                  },
                  child: Text(
                    'Arabic',
                    style: TextStyle(color: Colors.green),
                  ),
                ),
                title: Text(
                  'Language',
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Information',
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white54,
                borderRadius: BorderRadius.circular(20),
              ),
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ListTile(
                leading: Icon(Icons.assignment_late_outlined),
                // trailing: Icon(Icons.arrow_forward),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return AboutUsScreeen();
                    }));
                  },
                ),
                title: Text(
                  'About Us',
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white54,
                borderRadius: BorderRadius.circular(20),
              ),
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ListTile(
                leading: Icon(Icons.account_balance_rounded),
                trailing: IconButton(icon : Icon(Icons.arrow_forward),onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return ConditionsScreen();
                  }));
                },),
                title: Text(
                  'Terms and Conditions',
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white54,
                borderRadius: BorderRadius.circular(20),
              ),
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ListTile(
                leading: Icon(Icons.phone),
                trailing: IconButton(icon: Icon(Icons.arrow_forward),onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return ContactUsScreen();
                  }));
                },),
                title: Text(
                  'Contact Us',
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
