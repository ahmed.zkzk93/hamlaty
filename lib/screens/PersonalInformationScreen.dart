import 'package:flutter/material.dart';
import 'package:maravel/screens/SuccessScreen.dart';
import 'package:maravel/widget/CustomButton.dart';
import 'package:maravel/widget/CustomLabel.dart';
import 'package:maravel/widget/CustomTextField.dart';
import 'package:maravel/widget/CustomTitle.dart';

class PersonalInformationScreen extends StatefulWidget {
  @override
  _PersonalInformationScreenState createState() =>
      _PersonalInformationScreenState();
}

class _PersonalInformationScreenState extends State<PersonalInformationScreen> {

  bool firstValue = false;
  bool secondValue = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/bground.png'), fit: BoxFit.fill),
          ),
          child: ListView(
            children: [
              Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: Icon(Icons.arrow_back),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: CustomTitle(
                          text: 'المعلومات الشخصيه',
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  CustomLabel(
                    text: 'اسم المستفيد',
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomTextField(
                    text: 'الرجاء ادخال الاسم',
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomLabel(
                    text: 'رقم الجوال',
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomTextField(
                    text: 'الرجاء ادخال رقم الجوال',
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomLabel(
                    text: 'رقم الهويه',
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomTextField(
                    text: 'الرجاء ادخال رقم الهويه',
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomLabel(
                    text: 'الجنسيه',
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomTextField(
                    text: 'الرجاء ادخال الجنسيه',
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomLabel(
                    text: 'تاريخ الميلاد',
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          child: new TextField(
                              textAlign: TextAlign.end,
                              decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide(
                                      color: Colors.brown.withOpacity(0.5)),
                                ),
                                hintText: 'سنه',
                                hintStyle: TextStyle(fontSize: 14),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 5),
                              )),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Flexible(
                          child: new TextField(
                              textAlign: TextAlign.end,
                              decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide(
                                      color: Colors.brown.withOpacity(0.5)),
                                ),
                                hintText: 'شهر',
                                hintStyle: TextStyle(fontSize: 14),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 5),
                              )),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Flexible(
                          child: new TextField(
                              textAlign: TextAlign.end,
                              decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide(
                                      color: Colors.brown.withOpacity(0.5)),
                                ),
                                hintText: 'يوم',
                                hintStyle: TextStyle(fontSize: 14),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 5),
                              )),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomLabel(
                    text: 'مقر الاقامه الحاليه',
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(
                          width: 30,
                        ),
                        Flexible(
                          child: new TextField(
                              textAlign: TextAlign.end,
                              decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide(
                                      color: Colors.brown.withOpacity(0.5)),
                                ),
                                hintText: 'المدينه',
                                hintStyle: TextStyle(fontSize: 14),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 5),
                              )),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Flexible(
                          child: new TextField(
                              textAlign: TextAlign.end,
                              decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide(
                                      color: Colors.brown.withOpacity(0.5)),
                                ),
                                hintText: 'الدوله',
                                hintStyle: TextStyle(fontSize: 14),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 5),
                              )),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomLabel(
                    text: 'هل سبق لك الحجز',
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 60),
                        child: Row(
                          // mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              'لا',
                              style: TextStyle(fontSize: 20),
                            ),
                            Checkbox(
                              checkColor: Colors.white,
                              activeColor: Colors.amberAccent,
                              value: firstValue,
                              onChanged: (bool value) {
                                setState(() {
                                  firstValue = value;
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 210),
                            child: Text(
                              'نعم',
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                          Checkbox(
                            checkColor: Colors.white,
                            activeColor: Colors.amberAccent,
                            value: secondValue,
                            onChanged: (bool value) {
                              setState(() {
                                secondValue = value;
                              });
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10, right: 20),
                    height: 70,
                    child: TextField(
                      maxLines: 3,
                      textAlign: TextAlign.end,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(40),
                          borderSide:
                              BorderSide(color: Colors.brown.withOpacity(0.5)),
                        ),
                        hintText: 'برجاء ادخال المده',
                        hintStyle: TextStyle(fontSize: 14),
                        contentPadding: EdgeInsets.symmetric(vertical: 30, horizontal: 15),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomButton(text: 'ارسال', color: Colors.amberAccent, function: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return SuccessScreen();
                    }));
                  },)
                ],
              ),
            ],
          )),
    );
  }
}

// Container(
//   margin: EdgeInsets.only(left: 300, right: 20),
//   height: 45,
//   child: TextField(
//     textAlign: TextAlign.end,
//     decoration: InputDecoration(
//       enabledBorder: OutlineInputBorder(
//         borderRadius: BorderRadius.circular(20),
//         borderSide: BorderSide(color: Colors.brown.withOpacity(0.5)),
//       ),
//       hintText: 'يوم',
//       hintStyle: TextStyle(fontSize: 14),
//       contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
//     ),
//   ),
// ),
