import 'package:flutter/material.dart';

class TapBarController extends StatefulWidget {
  @override
  _TapBarControllerState createState() => _TapBarControllerState();
}

class _TapBarControllerState extends State<TapBarController> {
  int _bottomBarIndex = 0;
  int _tabBarIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _bottomBarIndex,
        onTap: (value) {
          setState(() {
            _bottomBarIndex = value;
          });
        },
        items: [
          BottomNavigationBarItem(
              title: Text('Test'), icon: Icon(Icons.person)),
          BottomNavigationBarItem(
              title: Text('Test'), icon: Icon(Icons.person)),
          BottomNavigationBarItem(
              title: Text('Test'), icon: Icon(Icons.person)),
          BottomNavigationBarItem(
              title: Text('Test'), icon: Icon(Icons.person)),
        ],
      ),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        bottom: TabBar(
          onTap: (value) {
            setState(() {
              _tabBarIndex = value;
            });
          },
          tabs: <Widget>[
            Text(
              'Jackets',
            ),
            Text(
              'Trousers',
            ),
            Text(
              'T-Shirts',
            ),
            Text(
              'Shoes',
            ),
          ],
        ),
      ),
      // body: TapBarController(),
    );
  }
}
