import 'package:flutter/material.dart';
import 'package:maravel/widget/CustomLabel.dart';
import 'package:maravel/widget/CustomLogo.dart';
import 'package:maravel/widget/CustomTextField.dart';
class ResetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bgroundsplash.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            CustomLogo(),
            SizedBox(
              height: 50,
            ),
            Padding(
              padding: EdgeInsets.only(right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'استعاده كلمه المرور',
                    style: TextStyle(fontSize: 18),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            CustomLabel(text: 'كلمه المرور'),
            SizedBox(
              height: 10,
            ),
            CustomTextField(
              text: 'الرجاء ادخال كلمه المرور',
              suffixIcon: Icon(Icons.phone_android),
              prefixIcon: Icon(Icons.visibility),
            ),
            SizedBox(
              height: 30,
            ),
            CustomLabel(text: 'تأكيد كلمه المرور'),
            SizedBox(
              height: 10,
            ),
            CustomTextField(
              text: 'الرجاء ادخال كلمه المرور',
              suffixIcon: Icon(Icons.phone_android),
              prefixIcon: Icon(Icons.visibility),
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Container(
                width: 400,
                height: 50,
                child: new RaisedButton(
                  color: Colors.amberAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: new Text(
                    'تأكيد',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/LoginScreen');
                  },
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
