import 'package:flutter/material.dart';
import 'package:maravel/widget/CustomLabel.dart';
import 'package:maravel/widget/CustomLogo.dart';
import 'package:maravel/widget/CustomTextField.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bgroundsplash.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            CustomLogo(),
            SizedBox(
              height: 30,
            ),
            CustomLabel(text: 'رقم الجوال'),
            CustomTextField(
              text: 'الرجاء ادخال رقم الجوال',
              suffixIcon: Icon(Icons.phone_android),
            ),
            SizedBox(
              height: 10,
            ),
            CustomLabel(
              text: 'كلمه المرور',
            ),
            SizedBox(
              height: 10,
            ),
            CustomTextField(
              text: 'الرجاء ادخال كلمه المرور',
              suffixIcon: Icon(Icons.lock),
              prefixIcon: Icon(Icons.visibility),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: Container(
                alignment: Alignment.topLeft,
                child: InkWell(
                  onTap: (){
                    Navigator.of(context).pushNamed('/phoneScreen');
                  },
                  child: Text('هل نسيت كلمه المرور؟'),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Container(
                width: 360,
                height: 50,
                child: new RaisedButton(
                  color: Colors.amberAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: new Text(
                    'دخول',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/searchScreen');
                  },
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed('/newRegister');
                      },
                      child: Text(
                        'تسجيل جديد',
                        style: TextStyle(color: Colors.amberAccent,fontSize: 16),
                      )),
                ),
                Text('ليس لديك حساب؟'),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
