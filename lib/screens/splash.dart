import 'package:flutter/material.dart';
import 'package:maravel/screens/lang.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/bgroundsplash.png'), fit: BoxFit.fill),
          ),
          child: Center(
            child: Image.asset(
              'images/splash.png',
              width: 220,
              height: 220,
              fit: BoxFit.contain,
            ),
          ),
        ),
        onTap: (){
          Navigator.of(context).pushNamed('/Lang');
        },
      ),
    );
  }
}

// Container(
//   decoration: BoxDecoration(
//     image: DecorationImage(
//         image: AssetImage('images/bgroundsplash.png'), fit: BoxFit.fill),
//   ),
//     child: Center(
//       child: Image.asset('images/splash.png',width: 220, height: 220, fit: BoxFit.contain,),
//     ),
// ),

// decoration: BoxDecoration(
// image: DecorationImage(
// image: AssetImage('images/bgroundsplash.png'),
// )
// ),

// AnimatedSplashScreen(
// splash: Image.asset(
// 'images/bgroundsplash.png',
// fit: BoxFit.fill,
// ),
// duration: 3000,
// nextScreen: Lang(),
// splashTransition: SplashTransition.fadeTransition,
// ),

// Center(
// child: Image.asset(
// 'images/splash.png',
// width: 220,
// height: 220,
// fit: BoxFit.contain,
// ),
// ),
