import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:maravel/screens/OrderDetailsScreen.dart';
import 'package:maravel/widget/CustomTitle.dart';

class OrderScreen extends StatefulWidget {
  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen>
    with TickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = new TabController(length: 3, vsync: this, initialIndex: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bground.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            CustomTitle(
              text: 'الطلبات',
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20), color: Colors.white),
              child: ListTile(
                title: TabBar(
                  indicatorColor: Colors.transparent,
                  controller: controller,
                  labelColor: Colors.amber,
                  unselectedLabelColor: Colors.black,
                  tabs: [
                    Tab(
                      text: 'Ended',
                    ),
                    Tab(
                      text: 'Recently',
                    ),
                    Tab(
                      text: 'Under Work',
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            InkWell(child: buildContainer(), onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return OrderDetailsScreen();
              }));
            },),
            SizedBox(
              height: 20,
            ),
            buildContainer(),
            SizedBox(
              height: 20,
            ),
            buildContainer(),
            SizedBox(
              height: 20,
            ),
            buildContainer(),
            SizedBox(
              height: 20,
            ),
            buildContainer(),
          ],
        ),
      ),
    );
  }

  Container buildContainer() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: ListTile(
        contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
        leading: Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/avatar.png'),
            ),
          ),
        ),
        title: Container(
          height: 40,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'The name of the service provider',
                style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 14.0,
                ),
              ),
              Text(
                'Order number',
                style: const TextStyle(
                  color: Colors.amber,
                  fontWeight: FontWeight.w600,
                  fontSize: 8.0,
                ),
              ),
            ],
          ),
        ),
        subtitle: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Display name',
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 10.0,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 220),
              child: Text(
                '####',
                style: const TextStyle(
                  color: Colors.amber,
                  fontWeight: FontWeight.w600,
                  fontSize: 10.0,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
