import 'package:flutter/material.dart';
import 'package:maravel/home_taps/Search.dart';
import 'package:maravel/widget/CustomButton.dart';

class SuccessScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bground.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            SizedBox(height: 100,),
            Image(
              image: AssetImage('images/avatar.png'),
              width: 350,
              height: 350,
            ),
            SizedBox(height: 20,),
            Text('تم ارسال العرض بنجاح', style: TextStyle(fontSize: 27),),
            SizedBox(height: 20,),
            Text('سيتم الرد عليكم من مقدم الخدمه التوصيل قريبا', style: TextStyle(fontSize: 20),),
            SizedBox(height: 250,),
            CustomButton(text: 'الرجوع للرئيسيه', color: Colors.amberAccent, function: (){
              Navigator.of(context).pushNamed('/searchScreen');
            },),
          ],
        ),
      ),
    );
  }
}
