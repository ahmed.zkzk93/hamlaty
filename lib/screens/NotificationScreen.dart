import 'package:flutter/material.dart';
import 'package:maravel/widget/CustomTitle.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bground.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: CustomTitle(text: 'الاشعارات',),
            ),
            SizedBox(
              height: 40,
            ),
            buildContainer(),
            SizedBox(height: 20,),
            buildContainer(),
            SizedBox(height: 20,),
            buildContainer(),
            SizedBox(height: 20,),
            buildContainer(),
            SizedBox(height: 20,),
            buildContainer(),
            SizedBox(height: 20,),
            buildContainer(),
            SizedBox(height: 20,),
            buildContainer(),
          ],
        ),
      ),
    );
  }

  Container buildContainer() {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: ListTile(
          // contentPadding: EdgeInsets.fromLTRB(100.0, 0.0, 10.0, 0.0),
          leading: CircleAvatar(
            backgroundColor: Colors.amberAccent,
            child: Icon(Icons.message),
          ),
          title: Text('Your request has been approved by the service provider',
            style: const TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 10,
            ),
          ),
          subtitle: Text('about 4 hours',
            style: const TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 10.0,
            ),
          ),
        ),

    );
  }
}
