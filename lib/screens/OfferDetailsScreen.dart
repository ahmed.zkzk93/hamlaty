import 'package:flutter/material.dart';
import 'package:maravel/screens/PersonalInformationScreen.dart';
import 'package:maravel/widget/CustomButton.dart';
import 'package:maravel/widget/CustomTitle.dart';

class OfferDetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        // height: MediaQuery.of(context).size.height,
        // width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/avatar.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.arrow_back),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: CustomTitle(
                      text: '',
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 200,
              ),
              Container(
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(40),
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 30, left: 20),
                            child: Text(
                              'Name Show',
                              style: TextStyle(color: Colors.amberAccent),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 240, top: 30),
                            child: Text('100 real'),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      ListTile(
                        title: Text(
                          'description',
                          style: TextStyle(fontSize: 24, color: Colors.amberAccent),
                        ),
                        subtitle: Text(
                          'any text here any text here any text here any text here any text here any text here any text here any text here any text here any text here any text here any text here any text here any text here any text here any text here  ',
                          style: TextStyle(
                            fontSize: 10,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left:15),
                            child: Text(
                              'Date Show',
                              style: TextStyle(color: Colors.amberAccent, fontSize: 24),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left:15),
                            child: Text(
                              'First Show From : 12 / 12/ 2021',
                              style: TextStyle(fontSize: 15),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left:15),
                            child: Text(
                              'Last Show From : 12 / 12/ 2021',
                              style: TextStyle(fontSize: 15),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 250,),
                      CustomButton(text: 'Book',color: Colors.amberAccent,function: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context){
                          return PersonalInformationScreen();
                        }));
                      },)
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
