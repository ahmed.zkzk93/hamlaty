import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:maravel/widget/CustomTitle.dart';

class ChangeLanguage extends StatefulWidget {
  @override
  _ChangeLanguageState createState() => _ChangeLanguageState();
}

class _ChangeLanguageState extends State<ChangeLanguage> {
  int radioValue1 = 0;
  // int radioValue2 = 0;
  // int radioValue3 = 0;

  // bool firstValue = false;
  // bool secondValue = false;
  // bool thirdValue = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bground.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: CustomTitle(
                    text: 'اللغه',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset(
                  'images/splash.png',
                  width: 70,
                  height: 70,
                ),
                Image.asset(
                  'images/splash.png',
                  width: 70,
                  height: 70,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    alignment: Alignment.topRight,
                    child: Text(
                      'العربيه',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  buildRadio(1),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    alignment: Alignment.topRight,
                    child: Text(
                      'English',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  buildRadio(2),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    alignment: Alignment.topRight,
                    child: Text(
                      'أوردو',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  buildRadio(3),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Radio<int> buildRadio(int value) {
    return Radio(
      activeColor: Colors.amberAccent,
      value: value,
      groupValue: radioValue1,
      onChanged: (value) {
        setState(() {
          radioValue1 = value;
        });
      },
    );
  }
}
