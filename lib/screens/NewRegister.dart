import 'package:flutter/material.dart';
import 'package:maravel/widget/CustomButton.dart';
import 'package:maravel/widget/CustomLabel.dart';
import 'package:maravel/widget/CustomLogo.dart';
import 'package:maravel/widget/CustomTextField.dart';

class NewRegister extends StatefulWidget {
  @override
  _NewRegisterState createState() => _NewRegisterState();
}

class _NewRegisterState extends State<NewRegister> {
  bool firstValue = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bgroundsplash.png'), fit: BoxFit.fill),
        ),
        child: ListView(
          children: [
            CustomLogo(),
            SizedBox(
              height: 30,
            ),
            CustomLabel(
              text: 'رقم الجوال',
            ),
            CustomTextField(
              text: 'الرجاء ادخال رقم الجوال',
              suffixIcon: Icon(Icons.phone_android),
            ),
            SizedBox(
              height: 10,
            ),
            CustomLabel(
              text: 'الاسم',
            ),
            SizedBox(
              height: 10,
            ),
            CustomTextField(
              text: 'الرجاء ادخال الاسم',
              suffixIcon: Icon(Icons.person_add),
            ),
            SizedBox(
              height: 10,
            ),
            CustomLabel(
              text: 'البريد الالكتروني',
            ),
            SizedBox(
              height: 10,
            ),
            CustomTextField(
              text: 'الرجاء ادخال البريد الالكتروني',
              suffixIcon: Icon(Icons.mail),
            ),
            SizedBox(
              height: 10,
            ),
            CustomLabel(
              text: 'الموقع',
            ),
            SizedBox(
              height: 10,
            ),
            CustomTextField(
              text: 'الرجاء ادخال الموقع',
              suffixIcon: Icon(Icons.location_on),
              prefixIcon: Icon(Icons.location_on),
            ),
            SizedBox(
              height: 10,
            ),
            CustomLabel(
              text: 'كلمه المرور',
            ),
            SizedBox(
              height: 10,
            ),
            CustomTextField(
              text: 'الرجاء ادخال كلمه المرور',
              suffixIcon: Icon(Icons.lock),
              prefixIcon: Icon(Icons.visibility),
            ),
            SizedBox(
              height: 10,
            ),
            CustomLabel(
              text: 'تأكيد كلمه المرور',
            ),
            SizedBox(
              height: 10,
            ),
            CustomTextField(
              text: 'الرجاء ادخال كلمه المرور',
              suffixIcon: Icon(Icons.lock),
              prefixIcon: Icon(Icons.visibility),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Container(
                      alignment: Alignment.topRight,
                      child: Text('قرأت ووافقت علي الشروط والأحكام'),
                    ),
                  ),
                  Checkbox(
                    checkColor: Colors.white,
                    activeColor: Colors.amberAccent,
                    value: firstValue,
                    onChanged: (bool value) {
                      setState(() {
                        firstValue = value;
                      });
                    },
                  ),
                ],
              ),
            ),
            CustomButton(
              text: 'تأكيد',
              color: Colors.amberAccent,
              function: () {
                Navigator.of(context).pushNamed('/confirmationCodeScreen');
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed('/LoginScreen');
                      },
                      child: Text(
                        'تسجيل دخول',
                        style:
                            TextStyle(color: Colors.amberAccent, fontSize: 16),
                      )),
                ),
                Text('لدي حساب؟'),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
