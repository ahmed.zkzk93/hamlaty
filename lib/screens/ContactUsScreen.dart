import 'package:flutter/material.dart';
import 'package:maravel/widget/CustomButton.dart';
import 'package:maravel/widget/CustomTitle.dart';
class ContactUsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bground.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: CustomTitle(
                    text: 'اتصل بنا',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset(
                  'images/splash.png',
                  width: 70,
                  height: 70,
                ),
                Image.asset(
                  'images/splash.png',
                  width: 70,
                  height: 70,
                ),
              ],
            ),
            SizedBox(height: 50,),
            Container(
              margin: EdgeInsets.all(30),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Write Your Message Here',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  )
                ),
                maxLines: 6,
                // enabled: true,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            CustomButton(text: 'Send', function: (){},color: Colors.amberAccent,),
            SizedBox(height: 250,),
            Column(
              children: [
                Text('or about social media',style: TextStyle(fontSize: 20),),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('images/facebook.png',width: 40, height: 40,),
                    Image.asset('images/twitter.png',width: 40, height: 40,),
                    Image.asset('images/instagram.png',width: 40, height: 40,),
                    Image.asset('images/whatsapp.png',width: 40, height: 40,),
                    Image.asset('images/youtube.png',width: 40, height: 40,),
                  ],
                ),

              ],
            )
          ],
        ),
      ),
    );
  }
}
