import 'package:flutter/material.dart';
import 'package:maravel/widget/CustomButton.dart';
import 'package:maravel/widget/CustomLabel.dart';
import 'package:maravel/widget/CustomTextField.dart';
import 'package:maravel/widget/CustomTitle.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class ProfilePersonlyScreen extends StatefulWidget {
  @override
  _ProfilePersonlyScreenState createState() => _ProfilePersonlyScreenState();
}

class _ProfilePersonlyScreenState extends State<ProfilePersonlyScreen> {
  void onButtonPressed() {
    showMaterialModalBottomSheet(
      context: context,
      builder: (context) => SingleChildScrollView(
        controller: ModalScrollController.of(context),
        child: Column(
          children: [
            Text(
              'change password',
              style: TextStyle(
                backgroundColor: Colors.amberAccent,
                fontSize: 40,
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            CustomTextField(
              text: 'old',
            ),
            CustomTextField(
              text: 'new',
            ),
            CustomTextField(
              text: 'confirm',
            ),
            SizedBox(
              height: 20,
            ),
            CustomButton(
              text: 'Confrim',
              color: Colors.amberAccent,
              function: () {
                Navigator.pop(context);
              },
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bground.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: CustomTitle(
                    text: 'الملف الشخصي',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 70,
            ),
            Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/ahmed.jpg'),
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            CustomLabel(
              text: 'رقم الجوال',
            ),
            SizedBox(
              height: 15,
            ),
            CustomTextField(
              suffixIcon: Icon(Icons.phone_android),
            ),
            SizedBox(
              height: 15,
            ),
            CustomLabel(
              text: 'الاسم',
            ),
            SizedBox(
              height: 15,
            ),
            CustomTextField(
              suffixIcon: Icon(Icons.person_add),
            ),
            SizedBox(
              height: 15,
            ),
            CustomLabel(
              text: 'البريد الالكتروني',
            ),
            SizedBox(
              height: 15,
            ),
            CustomTextField(
              suffixIcon: Icon(Icons.mail),
            ),
            SizedBox(
              height: 15,
            ),
            CustomLabel(
              text: 'الموقع',
            ),
            SizedBox(
              height: 15,
            ),
            CustomTextField(
              suffixIcon: Icon(Icons.location_on_outlined),
              prefixIcon: Icon(Icons.location_on_outlined),
            ),
            SizedBox(
              height: 15,
            ),
            CustomButton(
              text: 'تغيير كلمه المرور',
              function: () {
                onButtonPressed();
              },
              color: Colors.black,
            ),
            SizedBox(
              height: 15,
            ),
            CustomButton(
              text: 'تأكيد',
              function: () {
                Navigator.pop(context);
              },
              color: Colors.amberAccent,
            ),
          ],
        ),
      ),
    );
  }
}
