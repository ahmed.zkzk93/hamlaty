import 'package:flutter/material.dart';
import 'package:maravel/screens/OfferDetailsScreen.dart';
import 'package:maravel/widget/CustomTitle.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ProviderDetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bground.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: CustomTitle(
                    text: 'تفاصيل المقدم',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Image.asset(
              'images/avatar.png',
              height: 100,
              width: 100,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  RatingBarIndicator(
                    rating: 3,
                    itemBuilder: (context, index) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    itemCount: 5,
                    itemSize: 15,
                    direction: Axis.horizontal,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'اسم مقدم الخدمه',
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'الرياض- المملكه العربيه السعوديه',
                  style: TextStyle(fontSize: 10),
                ),
                Icon(
                  Icons.location_on_outlined,
                  size: 15,
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            ListTile(
              title: Text(
                'description',
                style: TextStyle(fontSize: 24, color: Colors.amberAccent),
              ),
              subtitle: Text(
                'any text here any text here any text here any text here any text here any text here any text here any text here any text here ',
                style: TextStyle(
                  fontSize: 10,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Offers',
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Flexible(
              child: GridView(
                children: [
                  InkWell(child: buildColumn(), onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return OfferDetailsScreen();
                    }));
                  },),
                  buildColumn(),
                  buildColumn(),
                  buildColumn(),
                  buildColumn(),
                  buildColumn(),
                  buildColumn(),
                  buildColumn(),
                  buildColumn(),
                ],
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 150,
                  childAspectRatio: 1,
                  mainAxisSpacing: 20,
                  crossAxisSpacing: 20,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Column buildColumn() {
    return Column(
                  children: [
                    Image(
                      image: AssetImage('images/avatar.png'),
                      width: 70,
                      height: 70,
                    ),
                    Text(
                      'اسم العرض',
                      style: TextStyle(fontSize: 14),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      '١٠٠ ريال',
                      style: TextStyle(color: Colors.amberAccent),
                    ),
                  ],
                );
  }
}

