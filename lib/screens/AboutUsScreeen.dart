import 'package:flutter/material.dart';
import 'package:maravel/widget/CustomTitle.dart';

class AboutUsScreeen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bground.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: CustomTitle(
                    text: 'من نحن',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset(
                  'images/splash.png',
                  width: 70,
                  height: 70,
                ),
                Image.asset(
                  'images/splash.png',
                  width: 70,
                  height: 70,
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            buildContainer(),
            SizedBox(height: 30,),
            buildContainer(),
            SizedBox(height: 30,),
            buildContainer(),
            SizedBox(height: 30,),
            buildContainer(),
            SizedBox(height: 30,),
            buildContainer(),
          ],
        ),
      ),
    );
  }

  Container buildContainer() {
    return Container(
            margin: EdgeInsets.only(left: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(
                  Icons.circle,
                  size: 10,
                  color: Colors.grey,
                ),
                SizedBox(
                  width: 5,
                ),
                Container(
                  // height: 40,
                  width: 380,
                  child: Text(
                      'Ahmed Talaat Zakzouk , Zain Ahmed Talaat Zakzouk , Hamza Ahmed Talaat Zakzouk,Ahmed Talaat Zakzouk , Zain Ahmed Talaat Zakzouk , Hamza Ahmed Talaat Zakzouk'),
                ),
              ],
            ),
          );
  }
}
