import 'package:flutter/material.dart';
import 'package:maravel/home_taps/Search.dart';
import 'package:maravel/screens/MoreScreen.dart';
import 'package:maravel/screens/NotificationScreen.dart';
import 'package:maravel/screens/OrderScreen.dart';
import 'package:maravel/screens/PhoneScreen.dart';
import 'package:maravel/screens/lang.dart';

import 'LoginScreen.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen>
    with TickerProviderStateMixin {
  TabController controller;

  void initState() {
    // TODO: implement initState
    super.initState();
    controller = new TabController(length: 4, vsync: this, initialIndex: 3);
  }

  int _currentIndex = 3;

  void onTabTapped(int index) {
    controller.animateTo(index);
    setState(() {
      _currentIndex = index;
    });
  }

  void datePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime(2021),
      firstDate: DateTime(1800),
      lastDate: DateTime(2050),
    );
  }

  bool firstValue = false;
  bool secondValue = false;
  bool thirdValue = false;
  bool fourthValue = false;
  bool fifthValue = false;
  bool sexValue = false;

  final imageList = [
    'https://cdn.pixabay.com/photo/2016/03/05/19/02/hamburger-1238246__480.jpg',
    'https://cdn.pixabay.com/photo/2016/11/20/09/06/bowl-1842294__480.jpg',
    'https://cdn.pixabay.com/photo/2017/01/03/11/33/pizza-1949183__480.jpg',
    'https://cdn.pixabay.com/photo/2017/02/03/03/54/burger-2034433__480.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      initialIndex: _currentIndex,
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/bground.png'), fit: BoxFit.fill),
          ),
          child: TabBarView(
            controller: controller,
            children: [
              MoreScreen(),
              NotificationScreen(),
              OrderScreen(),
              Search(),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          fixedColor: Colors.amber,
          unselectedItemColor: Colors.black,
          currentIndex: _currentIndex,
          type: BottomNavigationBarType.shifting,
          onTap: onTabTapped,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.calendar_today),
              title: Text('المزيد'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notification_important),
              title: Text('الاشعارات'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person_pin_outlined),
              title: Text('الطلبات'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              title: Text('بحث'),
            ),
          ],
        ),
      ),
    );
  }
}
