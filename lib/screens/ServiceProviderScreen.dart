import 'package:flutter/material.dart';
import 'package:maravel/screens/ProviderDetailsScreen.dart';
import 'package:maravel/widget/CustomTitle.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ServiceProviderScreen extends StatefulWidget {
  @override
  _ServiceProviderScreenState createState() => _ServiceProviderScreenState();
}

class _ServiceProviderScreenState extends State<ServiceProviderScreen> {
  String valueChoose;
  List listItem = ['الاقل تقييما', 'الاعلي تقييما', 'الاقرب'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bground.png'), fit: BoxFit.fill),
        ),
        child: ListView(
          children: [
            Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: DropdownButton(
                        value: valueChoose,
                        onChanged: (value) {
                          setState(() {
                            valueChoose = value;
                          });
                        },
                        items: listItem.map((valueItem) {
                          return DropdownMenuItem(
                            child: Text(valueItem),
                            value: valueItem,
                          );
                        }).toList(),
                        icon: Icon(Icons.add_road),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: CustomTitle(
                        text: 'مقدمين الخدمه',
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                // buildContainer(),
                InkWell(child: buildContainer(), onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return ProviderDetailsScreen();
                  }));
                },),
                SizedBox(
                  height: 12,
                ),
                buildContainer(),
                SizedBox(
                  height: 12,
                ),
                buildContainer(),
                SizedBox(
                  height: 12,
                ),
                buildContainer(),
                SizedBox(
                  height: 12,
                ),
                buildContainer(),
                SizedBox(
                  height: 12,
                ),
                buildContainer(),
                SizedBox(
                  height: 12,
                ),
                buildContainer(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Container buildContainer() {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: BoxDecoration(
        color: Colors.black26,
        borderRadius: BorderRadius.circular(20),
      ),
      child: ListTile(
        contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
        leading: Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/avatar.png'),
            ),
          ),
        ),
        title: Container(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'The name of the service provider',
                style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 14.0,
                ),
              ),
              RatingBarIndicator(
                rating: 3,
                itemBuilder: (context, index) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                itemCount: 5,
                itemSize: 10.0,
                direction: Axis.horizontal,
              ),
            ],
          ),
        ),
        subtitle: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Icon(
              Icons.location_on_outlined,
              size: 15,
            ),
            Text(
              'Suadi Arabia',
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 10.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
