import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maravel/screens/LoginScreen.dart';
import 'package:maravel/widget/CustomLogo.dart';

class Lang extends StatefulWidget {
  @override
  _LangState createState() => _LangState();
}

class _LangState extends State<Lang> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bgroundsplash.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            CustomLogo(),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: EdgeInsets.only(right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'اختر اللغه',
                    style: TextStyle(fontSize: 18),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Container(
                    width: 200,
                    height: 40,
                    decoration: BoxDecoration(color: Colors.amberAccent),
                    alignment: Alignment.topLeft,
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).pushNamed('/LoginScreen');
                        // Navigator.of(context).pushNamed('/TapBarController');
                      },
                      child: Center(
                        child: Text(
                          'العربيه',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Container(
                    width: 200,
                    height: 40,
                    decoration: BoxDecoration(color: Colors.black),
                    alignment: Alignment.topLeft,
                    child: InkWell(
                      onTap: () {
                        // Navigator.of(context).pushNamed('/LoginScreen');
                      },
                      child: Center(
                        child: Text(
                          'English',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Container(
                    width: 200,
                    height: 40,
                    decoration: BoxDecoration(color: Colors.grey),
                    alignment: Alignment.topLeft,
                    child: InkWell(
                      onTap: () {
                        // Navigator.of(context).pushNamed('/newRegister');
                      },
                      child: Center(
                        child: Text(
                          'أوردر',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
