import 'package:flutter/material.dart';
import 'package:maravel/widget/CustomLabel.dart';
import 'package:maravel/widget/CustomTextField.dart';
import 'package:maravel/widget/CustomTitle.dart';

class OrderDetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bground.png'), fit: BoxFit.fill),
        ),
        child: ListView(
          children: [
            Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.arrow_back),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: CustomTitle(
                        text: 'تفاصيل الطلب',
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                CustomLabel(
                  text: 'نتائج البحث',
                  color: Colors.amberAccent,
                ),
                SizedBox(
                  height: 10,
                ),
                ListTile(
                  title: Container(
                      padding: EdgeInsets.only(left: 350),
                      child: Text(
                        'القسم',
                        style: TextStyle(fontSize: 18),
                      )),
                  subtitle: Container(
                      padding: EdgeInsets.only(left: 365),
                      child: Text(
                        'حج',
                        style: TextStyle(fontSize: 14),
                      )),
                ),
                ListTile(
                  contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                  title: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 150),
                        child: Text(
                          'المدينه',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 145),
                        child: Text(
                          'الدوله',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                  subtitle: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 160),
                        child: Text(
                          'الرياض',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 110),
                        child: Text(
                          'المملكه العربيه السعوديه',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ListTile(
                  contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                  title: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 110),
                        child: Text(
                          'وسيله النقل',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 120),
                        child: Text(
                          'نوع الرحله',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                  subtitle: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 165),
                        child: Text(
                          'ملاكي',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 185),
                        child: Text(
                          'طيران',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ListTile(
                  contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                  title: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 100),
                        child: Text(
                          'تاريخ المغادره',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 80),
                        child: Text(
                          'تاريخ الوصول',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                  subtitle: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 130),
                        child: Text(
                          '12 / 12 / 2021',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 135),
                        child: Text(
                          '12 / 12 / 2021',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ListTile(
                  title: Container(
                    // margin: EdgeInsets.only(left: 300),
                      padding: EdgeInsets.only(left: 320),
                      child: Text(
                        'عدد النزلاء',
                        style: TextStyle(fontSize: 18),
                      )),
                  subtitle: Row(
                    children: [
                      Container(
                          padding: EdgeInsets.only(left:  200),
                          child: Text(
                            ' 2  أطفال',
                            style: TextStyle(fontSize: 14),
                          )),
                      Container(
                          padding: EdgeInsets.only(left: 95),
                          child: Text(
                            ' 2 أفراد  ',
                            style: TextStyle(fontSize: 14),
                          )),
                    ],
                  ),
                ),
                CustomLabel(
                  text: ' المعلومات الشخصيه',
                  color: Colors.amberAccent,
                ),
                ListTile(
                  contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 100),
                        child: Text(
                          'رقم الجوال',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 80),
                        child: Text(
                          'اسم المستفيد',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                  subtitle: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 90),
                        child: Text(
                          '029301011203379',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 15),
                        child: Text(
                          'محمد رمضان السيد',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ListTile(
                  contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 100),
                        child: Text(
                          'الجنسيه',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 80),
                        child: Text(
                          'تاريخ الميلاد',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                  subtitle: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 120),
                        child: Text(
                          'مصري',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 15),
                        child: Text(
                          '12 / 12 / 2021',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ListTile(
                  contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 0, 0.0),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 0),
                        child: Text(
                          'مدينه الاقامه الحاليه',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 30),
                        child: Text(
                          'دوله الاقامه الحاليه',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                  subtitle: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 150),
                        child: Text(
                          'المنصوره',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 20),
                        child: Text(
                          'مصر',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 5),
                  child: CustomLabel(
                    text: 'معلومات العرض',
                    color: Colors.amberAccent,
                  ),
                ),
                ListTile(
                  contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 20, 0.0),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 20),
                        child: Text(
                          'سعر العرض',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 80),
                        child: Text(
                          'اسم العرض',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                  subtitle: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 140),
                        child: Text(
                          '١٠٠ ريال',
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 5),
                        child: Text(
                          'اسم العرض',
                          style: const TextStyle(
                            // color: Colors.amber,
                            fontWeight: FontWeight.w600,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
