import 'package:flutter/material.dart';
import 'package:maravel/home_taps/Search.dart';
import 'package:maravel/screens/SearchScreen.dart';
import 'package:maravel/widget/CustomLabel.dart';
import 'package:maravel/widget/CustomLogo.dart';
import 'package:maravel/widget/CustomTextField.dart';

class ConfirmationCodeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bgroundsplash.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            CustomLogo(),
            SizedBox(
              height: 30,
            ),
            CustomLabel(text: 'ادخال كود التفعيل'),
            SizedBox(
              height: 10,
            ),
            CustomTextField(
              text: 'الرجاء ادخال كود التفعيل',
              suffixIcon: Icon(Icons.phone_android),
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Container(
                width: 400,
                height: 50,
                child: RaisedButton(
                  color: Colors.amberAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Text(
                    'تأكيد',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  onPressed: (){
                    Navigator.of(context).pushNamed('/searchScreen');
                  },
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'لم يصلك كود؟',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FlatButton(
                    onPressed: () {},
                    child: Text(
                      'اعاده ارسال',
                      style: TextStyle(
                        color: Colors.amberAccent,
                        fontSize: 17
                      ),
                    )),
              ],
            )
          ],
        ),
      ),
    );
  }
}
