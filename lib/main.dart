import 'package:flutter/material.dart';
import 'package:maravel/home_taps/Search.dart';
import 'package:maravel/screens/ChangeLanguage.dart';
import 'package:maravel/screens/CodeScreen.dart';
import 'package:maravel/screens/ConfirmationCodeScreen.dart';
import 'package:maravel/screens/LoginScreen.dart';
import 'package:maravel/screens/NewRegister.dart';
import 'package:maravel/screens/OrderScreen.dart';
import 'package:maravel/screens/PhoneScreen.dart';
import 'package:maravel/screens/ResetPassword.dart';
import 'package:maravel/screens/SearchScreen.dart';
import 'package:maravel/screens/TapBarController.dart';
import 'package:maravel/screens/lang.dart';
import 'screens/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/LoginScreen': (_) => LoginScreen(),
        '/Lang': (context) => Lang(),
        '/newRegister': (context) => NewRegister(),
        '/confirmationCodeScreen': (context) => ConfirmationCodeScreen(),
        '/phoneScreen': (context) => PhoneScreen(),
        '/codeScreen': (context) => CodeScreen(),
        '/resetPassword': (context) => ResetPassword(),
        '/searchScreen': (context) => SearchScreen(),
        '/TapBarController': (context) => TapBarController(),
        '/ChangeLanguage': (context) => ChangeLanguage(),
      },
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Splash(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
      ),
      body: null,
    );
  }
}

