import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Function function;
  final Color color;

  const CustomButton({this.text, this.function, this.color});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        width: 400,
        height: 50,
        child: new RaisedButton(
          color: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          child: new Text(
            text,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          onPressed: function,
        ),
      ),
    );
  }
}
