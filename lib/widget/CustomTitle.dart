import 'package:flutter/material.dart';
class CustomTitle extends StatelessWidget {
  final String text;

  const CustomTitle({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 20, top: 50),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            text,
            style: TextStyle(fontSize: 30),
          ),
        ],
      ),
    );
  }
}
