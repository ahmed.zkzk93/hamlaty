import 'package:flutter/material.dart';
class CustomLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(120, 120, 0, 0),
      child: Row(
        children: [
          Image.asset(
            'images/splash.png',
            width: 170,
            height: 170,
          ),
        ],
      ),
    );
  }
}
