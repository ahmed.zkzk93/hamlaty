import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String text;
  final Icon suffixIcon;
  final Icon prefixIcon;

  const CustomTextField({Key key, this.text, this.suffixIcon, this.prefixIcon})
      : super(key: key);

  // const CustomTextField({this.text, this.suffixIcon});
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
        child: Container(
          height: 45,
          alignment: Alignment.center,
          child: TextField(
            textAlign: TextAlign.end,
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                borderSide: BorderSide(color: Colors.brown.withOpacity(0.5)),
              ),
              hintText: text,
              hintStyle: TextStyle(fontSize: 14),
              contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
              suffixIcon: suffixIcon,
              prefixIcon: prefixIcon,
            ),
          ),
        ),
      ),
    );
  }
}


// leading: Container(
// decoration: BoxDecoration(
// image: DecorationImage(
// image: AssetImage('images/avatar.png'),
// ),
// ),
// ),