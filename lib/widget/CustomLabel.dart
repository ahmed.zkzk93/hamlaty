import 'package:flutter/material.dart';

class CustomLabel extends StatelessWidget {
  final String text;
  final Color color;

  const CustomLabel({ this.text,this.color});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(right: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              text,
              style: TextStyle(
                fontSize: 18,
                color: color,
              ),
            ),
          ],
        ),
    );
  }
}
